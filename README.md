# CIS 481 Homework 1

This homework was meant to show us how parallel programming works in Java Language. 

It was done in Eclipse IDE, but it is fairly easy to run without installing Eclipse.

The problem at hand is to transpose a Matrix T into Matrix M such that T[i, j] = M[i, j]. for all i and j.


- Write a parallel program using shared variables that computes the transpose of an n x n matrix M.  Use P worker processes.
For simplicity, assume that n is a multiple of P.
