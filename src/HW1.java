import java.util.concurrent.atomic.AtomicInteger;
//Team Leader: @Daniel_Mello
// and @Patrick_Simoes


public class HW1 implements Runnable{

	private static AtomicInteger finish = new AtomicInteger(0);
	private int taskID;
	private int first;
	private int last;
	private int n;
	int matrixm2[][];
	int matrixt2[][];
	
	

	
	
	public HW1() {}
	public HW1(int i, int first, int last, int n, int[][] matrixM, int[][] matrixT) {
		taskID = i; 
		this.first = first;
		this.last = last;
		this.n = n;
		matrixm2 = matrixM;
		matrixt2 = matrixT;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("TaskID: " + taskID);
		for(int i = first; i <= last; i++) {
			for(int j = 0; j < n; j++) {
				
				matrixt2[i][j] = matrixm2[j][i];
			}
		}
		
		finish.getAndAdd(1);
	}


	public static void main(String[] args) {

		int n = 10;
		int matrixM[][] = new int[n][n];
		int matrixT[][] = new int[n][n];
		int count = 0;
		for(int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				matrixM[i][j] = count;
				count++;
			}
		}
		int p = 5;
        Thread[] worker = new Thread[p+1];
        
        for (int w=1; w<=p; w++) {
        	int first = (w-1) * n/p;
        	int last = first + (n/p) - 1;
            worker[w]= new Thread(new HW1(w, first, last, n, matrixM, matrixT));
            worker[w].start();
        }
        
        while (finish.get() != p) { Thread.yield(); }
        System.out.println("Original matrix: ");
        for(int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				System.out.print(matrixM[i][j] + " ");
			}
			System.out.print("\n");
		}
        System.out.println("Results of transposed matrix: ");
        for(int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				System.out.print(matrixT[i][j] + " ");
			}
			System.out.print("\n");
		}
		
	}

	
}
